# frozen_string_literal: true

require 'gitlab'
require 'json'

GITLAB_ORG_GROUP_ID = 9970

class Node
  def to_h
    {
      name: name,
      children: children.map(&:to_h)
    }
  end

  def to_json
    JSON.pretty_generate(to_h)
  end
end

class Group < Node
  def initialize(client, group)
    @client = client
    @group = group
  end

  def id
    @group.id
  end

  def name
    @group.path
  end

  def children
    @children ||= subgroups + projects
  end

  private

  def subgroups
    @client
      .group_subgroups(id)
      .auto_paginate
      .filter_map do |subgroup|
        next unless subgroup.visibility == "public"

        puts "Group: #{subgroup.full_path}, Parent: #{@group.full_path}"
        $stdout.flush

        Group.new(@client, subgroup)
      end
  end

  def projects
    @client
      .group_projects(id)
      .auto_paginate
      .filter_map do |project|
        next unless project.visibility == "public"

        puts "Project: #{project.path_with_namespace}, Parent: #{@group.full_path}"
        $stdout.flush

        Project.new(project)
      end
  end
end

class Project < Node
  def initialize(project)
    @project = project
  end

  def name
    @project.path
  end

  def children
    []
  end
end

client = Gitlab.client(endpoint: 'https://gitlab.com/api/v4')
gitlab_org = client.group(GITLAB_ORG_GROUP_ID, with_projects: false)
root = Group.new(client, gitlab_org)
file = File.new('treeData.json', 'w')
file.write(root.to_json)
file.close
